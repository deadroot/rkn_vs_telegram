# -*- coding: utf-8 -*-
import socket
import binascii
import sys


DOMAIN_LIST = [ "telegram.org",
                "web.telegram.org",
                "telegram.me",
                "desktop.telegram.org",
                "telegram.dog",
                "venus.web.telegram.org",
                "pluto-1.web.telegram.org"
]

SUBNET_LIST = [ "91.108.4.0/22",
                "91.108.8.0/22",
                "91.108.12.0/22",
                "91.108.16.0/22",
                "91.108.56.0/22",
                "149.154.160.0/22",
                "149.154.164.0/22",
                "149.154.168.0/22",
                "35.192.0.0/12",
                "52.58.0.0/15",
                "18.196.0.0/15",
                "18.194.0.0/15",
                "35.156.0.0/14",
                "54.160.0.0/12",
                "35.208.0.0/12",
                "52.0.0.0/11",
                "34.192.0.0/10",
                "52.192.0.0/11"]


def ip_in_subnetwork(ip_address, subnetwork):
    (ip_integer, version1) = ip_to_integer(ip_address)
    (ip_lower, ip_upper, version2) = subnetwork_to_ip_range(subnetwork)
    if version1 != version2:
        raise ValueError("incompatible IP versions")
    return (ip_lower <= ip_integer <= ip_upper)

def ip_to_integer(ip_address):
    for version in (socket.AF_INET, socket.AF_INET6):
        try:
            ip_hex = socket.inet_pton(version, ip_address)
            ip_integer = int(binascii.hexlify(ip_hex), 16)
            return (ip_integer, 4 if version == socket.AF_INET else 6)
        except:
            pass
    raise ValueError("invalid IP address")

def subnetwork_to_ip_range(subnetwork):
    try:
        fragments = subnetwork.split('/')
        network_prefix = fragments[0]
        netmask_len = int(fragments[1])
        for version in (socket.AF_INET, socket.AF_INET6):
            ip_len = 32 if version == socket.AF_INET else 128
            try:
                suffix_mask = (1 << (ip_len - netmask_len)) - 1
                netmask = ((1 << ip_len) - 1) - suffix_mask
                ip_hex = socket.inet_pton(version, network_prefix)
                ip_lower = int(binascii.hexlify(ip_hex), 16) & netmask
                ip_upper = ip_lower + suffix_mask
                return (ip_lower,
                        ip_upper,
                        4 if version == socket.AF_INET else 6)
            except:
                pass
    except:
        pass
    raise ValueError("invalid subnetwork")


if __name__ == '__main__':
    try:
        type = sys.argv[1] # govnokod :)
        ip2check = sys.argv[2]
        if type == "ip":
            for subnet in SUBNET_LIST:
                if ip_in_subnetwork(ip2check, subnet):
                    print "RKN block IP {} with subnet {}. Check it here: https://blocklist.rkn.gov.ru".format(ip2check, subnet)
                else:
                    print "Looks should be fine. But who knows"
        else:
            print "Not ready yet :)"
    except:
        print "Example: 'python main.py ip 127.0.0.1'"
