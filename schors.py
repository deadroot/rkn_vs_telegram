import requests, json

SCHORS_URL = "https://usher2.club/d1_ipblock.json"

def totalCount():
    try:
        oneDayIPBlock = requests.get("{}".format(SCHORS_URL)).json()
        print "Total number of IPs banned by RKN: {}".format(oneDayIPBlock[len(oneDayIPBlock)-1]["y"])
    except Exception as error:
        print "Woops: {}".format(str(error))


if __name__ == '__main__':
    totalCount()
